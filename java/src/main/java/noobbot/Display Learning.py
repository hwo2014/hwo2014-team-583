import matplotlib.pyplot as plt

position = open("position.txt")
positionlist = list(map(lambda x: eval(x), position.read()[:-1].split("\n")))
position.close()

velocity = open("velocity.txt");
velocitylist = list(map(lambda x: eval(x), velocity.read()[:-1].split("\n")))
velocity.close()

acceleration = open("acceleration.txt");
accelerationlist = list(map(lambda x: eval(x), acceleration.read()[:-1].split("\n")))
acceleration.close()

throttle = open("throttle.txt");
throttlelist = list(map(lambda x: eval(x), throttle.read()[:-1].split("\n")))
throttle.close()

angle = open("angle.txt");
anglelist = list(map(lambda x: eval(x), angle.read()[:-1].split("\n")))
angle.close()

pp, = plt.plot(positionlist,"b-")
pv, = plt.plot(velocitylist,"r--")
pa, = plt.plot(accelerationlist,"g-")
pt, = plt.plot(throttlelist,"r-.")
pan, = plt.plot(anglelist,"b:")
plt.axis([0,len(positionlist),0,max(positionlist)])
plt.legend([pp, pv, pa, pt, pan],["position","velocity","acceleration","throttle","angle"])
plt.xlabel("ticks")
plt.title("data per tick")
plt.show()


