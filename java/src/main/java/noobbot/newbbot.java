package noobbot;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Deque;
import java.util.ArrayDeque;

import com.google.gson.Gson; //its fine

public class newbbot {
    public static void main(String... args) throws IOException {
        String host = args[0];
        int port = Integer.parseInt(args[1]);
        String botName = args[2];
        String botKey = args[3];

        System.out.println("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);

        final Socket socket = new Socket(host, port);
        final PrintWriter writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream(), "utf-8"));

        final BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream(), "utf-8"));

        new newbbot(reader, writer, new Join(botName, botKey));
    }

    final Gson gson = new Gson();
    private PrintWriter writer; 
    private Deque<Float> positions = new ArrayDeque<Float>();   //Java Util Doc said that Deque is better than Stack at being a stack
    private Deque<Double> totalDistanceList = new ArrayDeque<Double>();
    private Deque<Double> velocity = new ArrayDeque<Double>();
    private Deque<Double> acceleration = new ArrayDeque<Double>();
    private Piece[] pieces;
    private GameInitLane[] lanes;
    private double[][] pieceLenByLane;

    public newbbot(final BufferedReader reader, final PrintWriter writer, final Join join) throws IOException {
        this.writer = writer;
        String line = null;
        PrintWriter pos_file = new PrintWriter("position.txt", "UTF-8");
        PrintWriter vel_file = new PrintWriter("velocity.txt", "UTF-8");
        PrintWriter acc_file = new PrintWriter("acceleration.txt", "UTF-8");
        PrintWriter ang_file = new PrintWriter("angle.txt", "UTF-8");
        PrintWriter thr_file = new PrintWriter("throttle.txt", "UTF-8");
                
        send(join);

        while((line = reader.readLine()) != null) {
            final MsgWrapper msgFromServer = gson.fromJson(line, MsgWrapper.class);
            
            if (msgFromServer.msgType.equals("carPositions")) {
                CarPositions carPositions = gson.fromJson(line, CarPositions.class);
                if(carPositions.gameTick == 0){
                	positions.push(0f);
                	totalDistanceList.push(0d);
                    velocity.push(0d);
                }
                //float myAngle = carPositions.data[0].angle;
                float tempPosition = carPositions.data[0].piecePosition.inPieceDistance;
                double cumDist = tempPosition - positions.pop();
                positions.push(tempPosition);
                
            	acceleration.push(velocity.peek()); //push the previous velocity
            	velocity.push(totalDistanceList.peek());//push the previous distance
                
                if(cumDist >= 0) { //if you are proceeding as normal
                	totalDistanceList.push(cumDist + totalDistanceList.pop());
                } else { //if you are going from one track piece to another
                    totalDistanceList.push(totalDistanceList.pop() + carPositions.data[0].piecePosition.inPieceDistance + pieceLenByLane[(carPositions.data[0].piecePosition.pieceIndex-1+pieces.length)%pieces.length][carPositions.data[0].piecePosition.lane.startLaneIndex] - positions.peek());
                }

                velocity.push(totalDistanceList.peek());//push the new distance
                velocity.push(velocity.pop() - velocity.pop()); //push the difference in distances as the new velocity
            	acceleration.push(velocity.peek()); //push the new velocity
            	acceleration.push(acceleration.pop() - acceleration.pop()); //push the difference in velocities as the new acceleration
                
                System.out.println(totalDistanceList.peek());
                
                double throttle = 0.65;
                
                pos_file.println(totalDistanceList.peek());
                vel_file.println(velocity.peek());
                acc_file.println(acceleration.peek());
                ang_file.println(carPositions.data[0].angle);
                thr_file.println(throttle);
                send(new Throttle(throttle));
                
            } else if (msgFromServer.msgType.equals("join")) {
                System.out.println("Joined");
            } else if (msgFromServer.msgType.equals("gameInit")) {
                GameInit gameInit = gson.fromJson(line, GameInit.class);
                pieces = gameInit.data.race.track.pieces; //put all of the pieces into an array
                //make it so that each piece stores the proper length of the piece, finds the circumference of curved peices
                //doesn't yet worry about the length of switching lanes
                lanes = gameInit.data.race.track.lanes;
                
                pieceLenByLane = new double[pieces.length][lanes.length];
                for(int i=0; i < pieces.length; i++) {
                    for(int j=0; j < lanes.length; j++) {
                        if (pieces[i].length == 0) { //don't forget that pieces.length is the length of the array while pieces[i].length is the length of the piece
                            pieceLenByLane[i][j] = (pieces[i].radius - lanes[j].distanceFromCenter) * Math.toRadians(pieces[i].angle);
                        } else {
                            pieceLenByLane[i][j] = pieces[i].length;
                        }
                    }
                }
                System.out.println("Race init");
            } else if (msgFromServer.msgType.equals("gameEnd")) {
                System.out.println("Race end");
            } else if (msgFromServer.msgType.equals("gameStart")) {
                System.out.println("Race start");
            } else {
                send(new Ping());
                System.out.println("Message type: " + msgFromServer.msgType + " Data: " +  msgFromServer.data);   
            }
        }
        pos_file.close();
        vel_file.close();
        acc_file.close();
        ang_file.close();
        thr_file.close();
    }

    private void send(final SendMsg msg) {
        writer.println(msg.toJson());
        writer.flush();
    }
}

class MsgWrapper {
    public final String msgType;
    public final Object data;

    MsgWrapper(final String msgType, final Object data) {
        this.msgType = msgType;
        this.data = data;
    }

    public MsgWrapper(final SendMsg sendMsg) {
        this(sendMsg.msgType(), sendMsg.msgData());
    }
}

//---------------------------------------------------------------------------------------------------//
class GameInit {
    public String msgType;
    public GameInitData data;
}

class GameInitData {
    public GameInitRace race;
}

class GameInitRace {
    public Track track;
    public Car[] cars;
    public RaceSession raceSession;
}

class Track {
    public String id;
    public String name;
    public Piece[] pieces;
    public GameInitLane[] lanes;
    public StartingPoint startingPoint;
}

class Piece {
    public double length = 0;
    public boolean Switch = false;
    public double radius = 0;
    public double angle = 0;
}

class GameInitLane {
    public float distanceFromCenter;
    public int index;
}

class StartingPoint {
    public Position position;
    public float angle;
}

class Position {
    public float x;
    public float y;
}

class Car {
    public Id id;
    public Dimensions dimensions;
}

class Id {
    public String name;
    public String color;
}

class Dimensions {
    public float length;
    public float width;
    public float guideFlagPosition;

}

class RaceSession {
    public int laps;
    public int maxLapTimeMs;
    public boolean quickRace;
}
//----------------------------------------------------------------------------------------------------//
class CarPositions{
    public String msgType;
    public CarPositionsData[] data;
    public String gameId;
    public long gameTick;
}

class CarPositionsData {
    public Id id;
    public float angle;
    public PiecePosition piecePosition;
}

class PiecePosition {
    public int pieceIndex;
    public float inPieceDistance;
    public Lane lane;
    public int lap;
}

class Lane {
    public int startLaneIndex;
    public int endLaneIndex;
}

//-----------------------------------------------------------------------------------------------------//
abstract class SendMsg {
    public String toJson() {
        return new Gson().toJson(new MsgWrapper(this));
    }

    protected Object msgData() {
        return this;
    }

    protected abstract String msgType();
}

class Join extends SendMsg {
    public final String name;
    public final String key;

    Join(final String name, final String key) {
        this.name = name;
        this.key = key;
    }

    @Override
    protected String msgType() {
        return "join";
    }
}

class Ping extends SendMsg {
    @Override
    protected String msgType() {
        return "ping";
    }
}

//banana pie

class Throttle extends SendMsg {
    private double value;

    public Throttle(double value) {
        this.value = value;
    }

    @Override
    protected Object msgData() {
        return value;
    }

    @Override
    protected String msgType() {
        return "throttle";
    }
}

class SwitchLane extends SendMsg {
    private String value;

    public SwitchLane(String value) {
        this.value = value;
    }

    @Override
    protected Object msgData() {
        return value;
    }

    @Override
    protected String msgType() {
        return "switchLane";
    }
}